import { Module } from '@nestjs/common';
import { DatabaseModule } from '../db/database.module';
import { UsersModule } from '../users/users-module';
import { MigrationsModule } from '../migrations/migrations-module';

@Module({
  imports: [DatabaseModule, UsersModule, MigrationsModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
