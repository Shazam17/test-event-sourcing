import { v4 as uuidv4 } from 'uuid';

export const uuid4 = uuidv4;
