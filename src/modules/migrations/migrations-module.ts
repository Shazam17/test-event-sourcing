import { Module } from '@nestjs/common';
import { MigrationService } from './services/migration-service';
import { DatabaseModule } from "../db/database.module";

@Module({
  imports: [DatabaseModule],
  providers: [MigrationService],
})
export class MigrationsModule {}
