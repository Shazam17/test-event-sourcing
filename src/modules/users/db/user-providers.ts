import { DataSource } from 'typeorm';
import { UserSchema } from './user-schema';
import { EventSchema } from './event-schema';

export const getTokenName = (entity) =>
  `${entity.name.toUpperCase()}_REPOSITORY`;
export const userProviders = [
  {
    provide: getTokenName(UserSchema),
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(UserSchema),
    inject: ['DATA_SOURCE'],
  },
  {
    provide: getTokenName(EventSchema),
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(EventSchema),
    inject: ['DATA_SOURCE'],
  },
];
