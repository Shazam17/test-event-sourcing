import { Column, Entity, PrimaryColumn } from "typeorm";
import { UserStatusType } from "../domain-models/user-status-type";
import { UserDomainModel } from "../domain-models/user-domain-model";
import { uuid4 } from "../../app/uuid";
import { EventDomainModel } from "../domain-models/event-domain-model";

export enum TableName {
  USERS = 'users',
  EVENTS = 'events',
}

export enum EventType {
  CREATE_USER = 'CREATE_USER',
  UPDATE_USER = 'UPDATE_USER',
  DELETE_USER = 'DELETE_USER',
  RESTORE_USER = 'RESTORE_USER',
}

export class EventPayload {
  public readonly id: string;

  constructor(id: string) {
    this.id = id;
  }

  evolve(item: object) {
    throw new Error('Not implemented');
  }
}



export function evolveUser(events: EventPayload[]) {
  let item = null;
  for (const event of events) {
    item = event.evolve(item);
  }

  return item;
}

@Entity({
  name: TableName.EVENTS,
})
export class EventSchema {
  constructor(
    id: string,
    aggregateId: string,
    userId: string,
    model: string,
    type: EventType,
    payload: EventPayload,
    createdAt: Date,
    updatedAt: Date,
  ) {
    this.id = id;
    this.aggregateId = aggregateId;
    this.userId = userId;
    this.model = model;
    this.type = type;
    this.payload = payload;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  @PrimaryColumn({
    type: 'uuid',
  })
  id: string;

  @Column({ type: 'uuid' })
  aggregateId: string;

  @Column({ type: 'uuid', nullable: true })
  userId: string;

  @Column({ type: 'varchar' })
  model: string;

  @Column({ type: 'varchar' })
  type: EventType;

  @Column({
    type: 'jsonb',
  })
  payload: EventPayload;

  @Column({
    type: 'date',
  })
  createdAt: Date;

  @Column({
    type: 'date',
  })
  updatedAt: Date;
}
