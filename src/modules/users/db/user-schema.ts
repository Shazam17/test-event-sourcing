import { Entity, Column, PrimaryGeneratedColumn, PrimaryColumn } from 'typeorm';
import { UserStatusType } from '../domain-models/user-status-type';
import { TableName } from './event-schema';

@Entity({
  name: TableName.USERS,
})
export class UserSchema {
  constructor(
    id: string,
    name: string,
    status: UserStatusType,
    createdAt: Date,
    updatedAt: Date,
  ) {
    this.id = id;
    this.name = name;
    this.status = status;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  @PrimaryColumn({
    type: 'uuid',
  })
  id: string;

  @Column({ length: 500 })
  name: string;

  @Column({
    type: 'varchar',
  })
  status: UserStatusType;

  @Column({
    type: 'date',
  })
  createdAt: Date;

  @Column({
    type: 'date',
  })
  updatedAt: Date;
}
