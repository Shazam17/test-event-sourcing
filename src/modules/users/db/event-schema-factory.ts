import { EventSchema, EventType } from './event-schema';
import { EventDomainModel } from '../domain-models/event-domain-model';
import { Injectable } from '@nestjs/common';
import { CreateUserPayload } from '../domain-models/user-events/create-user-event';
import { UpdateUserPayload } from '../domain-models/user-events/update-user-event';
import { DeleteUserPayload } from '../domain-models/user-events/delete-user-event';
import { RestoreUserEventPayload } from '../domain-models/user-events/restore-user-event';

function eventPayloadSchemaFactory(eventType: EventType, eventPayload: any) {
  switch (eventType) {
    case EventType.CREATE_USER: {
      return CreateUserPayload.CreateFromDatabase(
        eventPayload.id,
        eventPayload.name,
        eventPayload.status,
        eventPayload.createdAt,
        eventPayload.updatedAt,
      );
    }
    case EventType.UPDATE_USER: {
      return UpdateUserPayload.CreateFromDatabase(
        eventPayload.id,
        eventPayload.name,
        eventPayload.updatedAt,
      );
    }
    case EventType.DELETE_USER: {
      return DeleteUserPayload.CreateFromDatabase(
        eventPayload.id,
        eventPayload.updatedAt,
      );
    }
    case EventType.RESTORE_USER: {
      return RestoreUserEventPayload.CreateFromDatabase(
        eventPayload.id,
        eventPayload.updatedAt,
      );
    }
    default:
      throw new Error(`Event: ${eventType} is not supported`);
  }
}

@Injectable()
export class EventSchemaFactory {
  fromSchema(schema: EventSchema): EventDomainModel {
    return new EventDomainModel(
      schema.id,
      schema.aggregateId,
      schema.userId,
      schema.model,
      schema.type,
      eventPayloadSchemaFactory(schema.type, schema.payload),
      schema.createdAt,
      schema.updatedAt,
    );
  }

  toSchema(model: EventDomainModel): EventSchema {
    return new EventSchema(
      model.id,
      model.aggregateId,
      model.userId,
      model.model,
      model.type,
      model.payload,
      model.createdAt,
      model.updatedAt,
    );
  }
}
