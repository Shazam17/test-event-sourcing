import { Injectable, Inject, NotFoundException } from "@nestjs/common";
import { Repository } from 'typeorm';
import { UserSchema } from './user-schema';
import { getTokenName } from './user-providers';
import { UserSchemaFactory } from './user-schema-factory';
import { UserDomainModel } from '../domain-models/user-domain-model';

@Injectable()
export class UsersRepository {
  constructor(
    @Inject(getTokenName(UserSchema))
    private usersRepository: Repository<UserSchema>,
    private readonly usersSchemaFactory: UserSchemaFactory,
  ) {}

  async findAll(): Promise<UserDomainModel[]> {
    const items = await this.usersRepository.find();
    return items.map((item) => this.usersSchemaFactory.fromSchema(item));
  }

  async save(user: UserDomainModel) {
    await this.usersRepository.save(this.usersSchemaFactory.toSchema(user));
  }

  async findById(id: string) {
    const item = await this.usersRepository.findOneBy({
      id: id,
    });

    if (item) {
      return this.usersSchemaFactory.fromSchema(item);
    } else {
      throw new NotFoundException(`User with id:${id} not found`);
    }
  }

  async delete(id: string) {
    await this.usersRepository.delete(id);
  }
}
