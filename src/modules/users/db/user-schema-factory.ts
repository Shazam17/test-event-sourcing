import { UserSchema } from './user-schema';
import { UserDomainModel } from '../domain-models/user-domain-model';
import { Injectable } from '@nestjs/common';

@Injectable()
export class UserSchemaFactory {
  fromSchema(schema: UserSchema): UserDomainModel {
    return new UserDomainModel(
      schema.id,
      schema.name,
      schema.status,
      schema.createdAt,
      schema.updatedAt,
    );
  }

  toSchema(model: UserDomainModel): UserSchema {
    return new UserSchema(
      model.id,
      model.name,
      model.status,
      model.createdAt,
      model.updatedAt,
    );
  }
}
