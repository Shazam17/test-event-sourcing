import { Injectable, Inject, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { getTokenName } from './user-providers';
import { EventSchema } from './event-schema';
import { EventSchemaFactory } from './event-schema-factory';
import { EventDomainModel } from '../domain-models/event-domain-model';

@Injectable()
export class EventsRepository {
  constructor(
    @Inject(getTokenName(EventSchema))
    private eventSchemaRepository: Repository<EventSchema>,
    private eventsSchemaFactory: EventSchemaFactory,
  ) {}

  async findAll(): Promise<EventSchema[]> {
    return this.eventSchemaRepository.find();
  }

  async save(event: EventDomainModel) {
    await this.eventSchemaRepository.save(
      this.eventsSchemaFactory.toSchema(event),
    );
  }

  async findById(id: string) {
    const item = await this.eventSchemaRepository.findOneBy({
      id: id,
    });

    if (item) {
      return this.eventsSchemaFactory.fromSchema(item);
    } else {
      throw new NotFoundException(`Event with id:${id} not found`);
    }
  }

  async findEvents(name: string, id: string) {
    const item = await this.eventSchemaRepository.find({
      where: {
        model: name,
        aggregateId: id,
      },
      order: {
        createdAt: 'asc',
      },
    });

    return item.map((item) => this.eventsSchemaFactory.fromSchema(item));
  }
}
