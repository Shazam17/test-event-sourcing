export enum UserStatusType {
  ACTIVE = 'ACTIVE',
  DELETED = 'DELETED',
}