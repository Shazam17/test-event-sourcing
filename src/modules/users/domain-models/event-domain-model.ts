import { EventPayload, EventSchema, EventType } from '../db/event-schema';
import { uuid4 } from '../../app/uuid';
import { UserDomainModel } from './user-domain-model';

export class EventDomainModel {
  constructor(
    id: string,
    aggregateId: string,
    userId: string,
    model: string,
    type: EventType,
    payload: EventPayload,
    createdAt: Date,
    updatedAt: Date,
  ) {
    this.id = id;
    this.aggregateId = aggregateId;
    this.userId = userId;
    this.model = model;
    this.type = type;
    this.payload = payload;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  public id: string;
  public aggregateId: string;
  public userId: string;
  public model: string;
  public type: EventType;
  public payload: EventPayload;
  public createdAt: Date;
  public updatedAt: Date;

  static FromPayload(
    userEventPayload: EventPayload,
    model: string,
    eventType: EventType,
  ) {
    const now = new Date();

    return new EventDomainModel(
      uuid4(),
      userEventPayload.id,
      null,
      UserDomainModel.name,
      eventType,
      userEventPayload,
      now,
      now,
    );
  }
}
