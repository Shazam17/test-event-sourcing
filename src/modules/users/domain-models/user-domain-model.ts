import { UserStatusType } from './user-status-type';

export class UserDomainModel {
  constructor(
    id: string,
    name: string,
    status: UserStatusType,
    createdAt: Date,
    updatedAt: Date,
  ) {
    this.id = id;
    this.name = name;
    this.status = status;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  public id: string;
  public name: string;
  public status: UserStatusType;
  public createdAt: Date;
  public updatedAt: Date;
}
