import { UserStatusType } from "../user-status-type";
import { uuid4 } from "../../../app/uuid";
import { UserDomainModel } from "../user-domain-model";
import { EventDomainModel } from "../event-domain-model";
import { EventPayload, EventType } from "../../db/event-schema";

export class CreateUserPayload extends EventPayload {
  private constructor(
    id: string,
    name: string,
    status: UserStatusType,
    createdAt: Date,
    updatedAt: Date,
  ) {
    super(id);
    this.name = name;
    this.status = status;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
  public name: string;
  public status: UserStatusType;
  public createdAt: Date;
  public updatedAt: Date;

  static Create(name: string) {
    const now = new Date();

    return new CreateUserPayload(
      uuid4(),
      name,
      UserStatusType.ACTIVE,
      now,
      now,
    );
  }

  static CreateFromDatabase(
    id: string,
    name: string,
    status: UserStatusType,
    createdAt: Date,
    updatedAt: Date,
  ) {
    return new CreateUserPayload(id, name, status, createdAt, updatedAt);
  }

  evolve() {
    return new UserDomainModel(
      this.id,
      this.name,
      this.status,
      this.createdAt,
      this.updatedAt,
    );
  }

  createEventModel() {
    return EventDomainModel.FromPayload(
      this,
      UserDomainModel.name,
      EventType.CREATE_USER,
    );
  }
}