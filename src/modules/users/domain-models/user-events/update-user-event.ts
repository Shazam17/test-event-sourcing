import { UserStatusType } from '../user-status-type';
import { UserDomainModel } from '../user-domain-model';
import { EventDomainModel } from '../event-domain-model';
import { EventPayload, EventType } from '../../db/event-schema';

export class UpdateUserPayload extends EventPayload {
  public readonly name: string;
  public readonly status: UserStatusType;
  public readonly updatedAt: Date;

  private constructor(userId: string, name: string, updatedAt: Date) {
    super(userId);
    this.name = name;
    this.updatedAt = updatedAt;
  }

  static CreateFromDatabase(userId: string, name: string, updatedAt: Date) {
    return new UpdateUserPayload(userId, name, updatedAt);
  }

  static Create(userId: string, name: string) {
    const now = new Date();
    return new UpdateUserPayload(userId, name, now);
  }
  evolve(item: UserDomainModel) {
    item.name = this.name;
    item.updatedAt = this.updatedAt;

    return item;
  }

  createEventModel() {
    return EventDomainModel.FromPayload(
      this,
      UserDomainModel.name,
      EventType.UPDATE_USER,
    );
  }
}
