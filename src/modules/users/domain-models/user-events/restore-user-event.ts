import { UserDomainModel } from '../user-domain-model';
import { UserStatusType } from '../user-status-type';
import { EventDomainModel } from '../event-domain-model';
import { EventPayload, EventType } from '../../db/event-schema';

export class RestoreUserEventPayload extends EventPayload {
  public readonly updatedAt: Date;

  private constructor(userId: string, updatedAt: Date) {
    super(userId);
    this.updatedAt = updatedAt;
  }

  static CreateFromDatabase(userId: string, updatedAt: Date) {
    return new RestoreUserEventPayload(userId, updatedAt);
  }

  static Create(userId: string) {
    const now = new Date();
    return new RestoreUserEventPayload(userId, now);
  }
  evolve(item: UserDomainModel) {
    item.status = UserStatusType.ACTIVE;
    item.updatedAt = this.updatedAt;
    return item;
  }

  createEventModel() {
    return EventDomainModel.FromPayload(
      this,
      UserDomainModel.name,
      EventType.RESTORE_USER,
    );
  }
}
