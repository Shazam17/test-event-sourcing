import { UserDomainModel } from '../user-domain-model';
import { UserStatusType } from '../user-status-type';
import { EventDomainModel } from '../event-domain-model';
import { EventPayload, EventType } from '../../db/event-schema';

export class DeleteUserPayload extends EventPayload {
  public readonly updatedAt: Date;

  private constructor(userId: string, updatedAt: Date) {
    super(userId);
    this.updatedAt = updatedAt;
  }

  static CreateFromDatabase(userId: string, updatedAt: Date) {
    return new DeleteUserPayload(userId, updatedAt);
  }

  static Create(userId: string) {
    const now = new Date();
    return new DeleteUserPayload(userId, now);
  }
  evolve(item: UserDomainModel) {
    item.status = UserStatusType.DELETED;
    item.updatedAt = this.updatedAt;
    return item;
  }

  createEventModel() {
    return EventDomainModel.FromPayload(
      this,
      UserDomainModel.name,
      EventType.DELETE_USER,
    );
  }
}
