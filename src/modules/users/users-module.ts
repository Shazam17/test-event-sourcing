import { Module } from '@nestjs/common';
import { userProviders } from './db/user-providers';
import { UsersRepository } from './db/users-repository';
import { DatabaseModule } from '../db/database.module';
import { EventsRepository } from './db/events-repository';
import { UsersController } from './api/users-controller';
import { EventSchemaFactory } from './db/event-schema-factory';
import { UserSchemaFactory } from './db/user-schema-factory';
import { UserService } from "./services/user-service";

@Module({
  imports: [DatabaseModule],
  controllers: [UsersController],
  providers: [
    ...userProviders,
    UsersRepository,
    EventsRepository,
    EventSchemaFactory,
    UserSchemaFactory,
    UserService
  ],
})
export class UsersModule {}
