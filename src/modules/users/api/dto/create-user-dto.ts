import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty} from 'class-validator';
export class CreateUserRequestDto {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
}
