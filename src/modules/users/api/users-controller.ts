import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe, Patch,
  Post,
  Put
} from "@nestjs/common";
import { UserService } from '../services/user-service';
import { CreateUserRequestDto } from './dto/create-user-dto';
import { UpdateUserDto } from './dto/update-user-dto';

@Controller({
  path: 'users',
})
export class UsersController {
  constructor(private readonly userService: UserService) {}
  @Post('')
  public createUser(@Body() createUserDto: CreateUserRequestDto) {
    this.userService.createUser(createUserDto.name);
  }

  @Get('all')
  public findUsers() {
    return this.userService.getAll();
  }

  @Get('all-events')
  public findAllEvents() {
    return this.userService.getAllEvents();
  }

  @Put(':id')
  public updateUser(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return this.userService.updateUser(id, updateUserDto.name);
  }

  @Delete(':id')
  public deleteUser(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.userService.deleteUser(id);
  }

  @Patch('restore/:id')
  public restoreUser(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.userService.restoreUser(id);
  }
}
