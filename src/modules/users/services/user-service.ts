import { Injectable } from '@nestjs/common';
import { UsersRepository } from '../db/users-repository';
import { UserDomainModel } from '../domain-models/user-domain-model';
import { UserStatusType } from '../domain-models/user-status-type';
import { evolveUser } from '../db/event-schema';
import { EventsRepository } from '../db/events-repository';
import { CreateUserPayload } from '../domain-models/user-events/create-user-event';
import { UpdateUserPayload } from '../domain-models/user-events/update-user-event';
import { DeleteUserPayload } from '../domain-models/user-events/delete-user-event';
import { RestoreUserEventPayload } from '../domain-models/user-events/restore-user-event';

@Injectable()
export class UserService {
  constructor(
    private readonly userRepository: UsersRepository,
    private readonly eventsRepository: EventsRepository,
  ) {}

  getAll() {
    return this.userRepository.findAll();
  }

  getAllEvents() {
    return this.eventsRepository.findAll();
  }

  async createUser(name: string) {
    const eventPayload = CreateUserPayload.Create(name);
    await this.eventsRepository.save(eventPayload.createEventModel());

    return this.evolveUserById(eventPayload.id);
  }

  async updateUser(id: string, name: string) {
    const user = await this.userRepository.findById(id);
    const updateUserPayload = UpdateUserPayload.Create(id, name);
    await this.eventsRepository.save(updateUserPayload.createEventModel());

    return this.evolveUserById(user.id);
  }

  async deleteUser(id: string) {
    const user = await this.userRepository.findById(id);
    const deleteUserPayload = DeleteUserPayload.Create(id);
    await this.eventsRepository.save(deleteUserPayload.createEventModel());

    return this.userRepository.delete(user.id);
  }

  async restoreUser(id: string) {
    const restoreUserEventPayload = RestoreUserEventPayload.Create(id);
    await this.eventsRepository.save(
      restoreUserEventPayload.createEventModel(),
    );

    return this.evolveUserById(restoreUserEventPayload.id);
  }

  async evolveUserById(userId: string) {
    const userEvents = await this.eventsRepository.findEvents(
      UserDomainModel.name,
      userId,
    );

    const evolvedUser = evolveUser([...userEvents.map((item) => item.payload)]);

    return this.userRepository.save(evolvedUser);
  }
}
