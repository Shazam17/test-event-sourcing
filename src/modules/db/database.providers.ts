import { DataSource } from 'typeorm';

export const databaseProviders = [
  {
    provide: 'DATA_SOURCE',
    useFactory: async () => {
      const dataSource = new DataSource({
        type: 'postgres',
        host: 'localhost',
        port: 5432,
        synchronize: true,
        schema: 'test-crm',
        username: 'admin',
        password: 'password',
        database: 'postgres',
        entities: [__dirname + '/../**/*-schema{.ts,.js}'],
      });

      return dataSource.initialize();
    },
  },
];
