import { NestFactory } from '@nestjs/core';
import { AppModule } from './modules/app/app.module';
import { ConfigModule } from '@nestjs/config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  ConfigModule.forRoot({
    envFilePath: '.env',
  });

  const swaggerConfig = new DocumentBuilder().build();

  const app = await NestFactory.create(AppModule);
  const document = SwaggerModule.createDocument(app, swaggerConfig);

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
    }),
  );

  SwaggerModule.setup('v1/swagger/api-docs', app, document, {});
  console.log(
    `swagger docs available at: http://localhost:3000/v1/swagger/api-docs`,
  );
  await app.listen(3000);
}
bootstrap();
